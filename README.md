Role "Selfhost-Server"
=========

Used to setup "selfhost-server" spec on a bootstrapped server

Molecule test: [![pipeline status](https://gitlab.com/ansible-achadwick/roles/selfhost-server/badges/master/pipeline.svg)](https://gitlab.com/ansible-achadwick/roles/selfhost-server/-/commits/master)

Required Variables
----------------
These variables are not set within the role. They have no default value. You must set these in the playbook which uses the role.

letsencrypt_staging:
  - value=0, letsencrypt will create production certificates OR
  - value=1, letsencrypt will create test certificates

godaddy_api:
  - key: <key-value>
  - secret: <secret-value>

mysql:
  - root_password: <password>
  - database: nextcloud
  - user: nextcloud
  - password: <password>

plex:
  - user: <plex-username>
  - password: <plex-password>

parent_domain: <domain>, this is used in letsencrypt script and nginx config. Will create subdomains of this domain.

See the example playbook below for more info

Example Playbook
----------------

```
---
- name: Configure selfhost server on existing Docker host
  hosts: test
  remote_user: ansible
  become: yes
  vars_files: vars.yml
  vars:
    - ansible_ssh_private_key_file: "{{ ansible_ssh_private_key }}"
    - parent_domain: dwick.co.uk
    - letsencrypt_staging: 0
    - godaddy_api:
        key: <key-value>
        secret: <secret-value>
    - mysql:
        root_password: <password>
        database: nextcloud
        user: nextcloud
        password: <password>
    - plex:
        user: <plex-username>
        password: <plex-password>

  roles:
    - setup             #Bootstrap a newly provisioned server
    - docker-ce         #Setup docker-ce and docker-compose
    - selfhost-server
```

Include this role using an Ansible Galaxy requirements.yml file. E.g.:
```
- src: git+https://gitlab.com/ansible-achadwick/roles/selfhost-server.git
  scm: git
  version: "0.1.7"  # quoted, so YAML doesn't parse this as a floating-point value
  name: selfhost-server
```
And running:
```
ansible-galaxy role install -r requirements.yml
```

License
-------

BSD
